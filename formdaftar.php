<?php include "connect.php";?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Farmagic</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
  </head>
  <body>
<nav class="navbar navbar-expand-lg bg-success">
  <div class="container-fluid container">
    <a class="navbar-brand" href="formdaftar.php">Daftar Admin Farmagic</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
            <div class="container">
                <a class="nav-link active" aria-current="page" href="index.html">Halaman Web</a>
            </div>
        </li>
        <?php if (empty($_SESSION['username'])) {?>
        <?php }else{ ?>

        <li class="nav-item">
            <div class="container">
                <a class="nav-link" href="formadd.php">Admin Berita</a>
            </div>
        </li>
        <?php } ?>
      </ul>
      

      <?php if (empty($_SESSION['username'])) {?>
      
      <a href="formlogin.php" class="btn btn-light">Masuk</a>

      <?php }else{ ?>
      <a href="logout.php" class="btn btn-danger">Keluar</a>

      <?php } ?>

    </div>
  </div>
</nav>

<div class ='container'>
<h1>Daftar</h1>
<form method='post' action="saveuser.php">
    <div class="row">
        <div class="col">
            <div class="mb-3 mt-5">
                <label class="form-label">Username</label>
                <input type="text" class="form-control" placeholder="Username" name="username" required>
            </div>
            <div class="mb-3">
                <label class="form-label">Email</label>
                <input type="text" class="form-control" placeholder="Email" name="email" required>
            </div>
            <div class="mb-3">
                <label class="form-label">Nama</label>
                <input type="text " class="form-control" placeholder="nama" name="nama" required>
            </div>
            <div class="mb-3">
                <label class="form-label">Password</label>
                <input type="password" class="form-control" placeholder="password" name="password"  required>
            </div>
            <div class="mb-5 d-grid gap-2 col-6 mx-auto">
                <input id="button" type="submit" class="btn btn-success text-light mt-5 shadow" style="--bs-btn-border-radius: 30rem;" value="Daftar" width="20px">
            </div>
        </div>
    </div>
</form>
</div>
</div>
</div>
</div>

    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.5/dist/umd/popper.min.js" integrity="sha384-Xe+8cL9oJa6tN/veChSP7q+mnSPaj5Bcu9mPX5F5xIGE0DVittaqT5lorf0EI7Vk" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.min.js" integrity="sha384-ODmDIVzN+pFdexxHEHFBQH3/9/vQ9uori45z4JjnFsRydbmQbmL5t1tQ0culUzyK" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
  </body>


</html>