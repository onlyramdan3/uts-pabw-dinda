<?php include "connect.php";?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Farmagic</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
    <!-- data Fuison -->
    <script type="text/javascript" src="./js/fusioncharts.js"></script>
    <script type="text/javascript" src="./js/themes/fusioncharts.theme.fint.js"></script>
        <script type="text/javascript">
            FusionCharts.ready(function(){
                    var revenueChart = new FusionCharts({
                        "type":"column2d",
                        "renderAt":"datafusion",
                        "width": "1000",
                        "height":"400",
                        "dataFormat":"jsonurl",
                        "dataSource": "./db2json.php"
                    });
                    revenueChart.render();
                }
            )
      </script>
  </head>
  <body>
<nav class="navbar navbar-expand-lg bg-success">
  <div class="container-fluid container">
    <a class="navbar-brand" href="formlogin.php">Login Admin Farmagic</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
            <div class="container">
                <a class="nav-link active" aria-current="page" href="index.html">Halaman Web</a>
            </div>
        </li>
        <?php if (empty($_SESSION['username'])) {?>
        <?php }else{ ?>

        <li class="nav-item">
            <div class="container">
                <a class="nav-link" href="formadd.php">Admin Berita</a>
            </div>
        </li>
        <?php } ?>
      </ul>
      <?php if (empty($_SESSION['username'])) {?>
      
      <a href="formlogin.php" class="btn btn-light">Masuk</a>

      <?php }else{ ?>
      <a href="logout.php" class="btn btn-danger">Keluar</a>

      <?php } ?>

    </div>
  </div>
</nav>
<div class="container">
<?php
if(isset($_SESSION['pesan'])){

  echo "<h4>".$_SESSION['pesan']."</h4>";
  unset($_SESSION['pesan']);
}
?>
<form action="add.php" method="post"  enctype="multipart/form-data">
<div class="mb-3">
  <label for="formFile" class="form-label">Import File</label>
  <input class="form-control" type="file" id="formFile" name="exel_file">
  <button type="submit" class="btn btn-success mt-3" name="save_exel_file">Simpan</button>
</div>
</form>
<div class="container text-center">
  <div id="datafusion"></div>
</div>
<div class="text-center mt-3 mb-3">
  <h6>Data Dalam Tabel</h6>
</div>
<table class="table">
  <thead class="table-success">
    <tr>
        <th>No.</th>
        <th>ID Berita</th>
        <th>Judul Berta</th>
        <th>Kategori</th>
        <th>Gambar</th>
        <th>Aksi</th>
        
    </tr>
  </thead>
  <tbody>
    <?php
    $sql ='SELECT * FROM berita join kberita on kberita.id_kategori = berita.id_kategori';
    $query = mysqli_query($conn, $sql) ;
    $i = 1;

    while ($row = mysqli_fetch_object($query)) {
    ?>
    <tr>
        <td><?php echo $i++, '.';?></td>
        <td><?php echo $row->id_berita; ?></td>
        <td><?php echo $row->judul; ?></td>
        <td><?php echo $row->nama_kategori; ?></td>
        <td><img width="200px" src="./gambar/<?php echo $row->gambar?>" alt=""></td>
        <?php if (!empty($_SESSION['username']) ) {?>
        <td>
          <a href="edit.php?id_berita=<?php echo $row->id_berita;?>" class='btn btn-warning btn-sm'>Ubah</a>
          
          <a href="deleteberita.php?id_berita=<?php echo $row->id_berita;?>" class='btn btn-danger btn-sm' onclick="return confirm('Apakah anda yakin untuk menghapus berita?')">hapus</a>
        </td>
        <?php } ?>

    </tr>
    <?php
    }
    if (! mysqli_num_rows($query)){
            echo '<tr> <td colspan="6" class="text-center"> TIDAK ADA DATA </td></tr>';
    }
    ?>
  </tbody>
</table>

</div>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.5/dist/umd/popper.min.js" integrity="sha384-Xe+8cL9oJa6tN/veChSP7q+mnSPaj5Bcu9mPX5F5xIGE0DVittaqT5lorf0EI7Vk" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.min.js" integrity="sha384-ODmDIVzN+pFdexxHEHFBQH3/9/vQ9uori45z4JjnFsRydbmQbmL5t1tQ0culUzyK" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
  </body>


</html>